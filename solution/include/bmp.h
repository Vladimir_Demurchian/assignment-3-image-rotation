#ifndef BMP_H
#define BMP_H

#include <stdint.h>
#include <stdio.h>

#include "image.h"

#define PADDING 4

struct __attribute__((packed)) bmp_header {
  uint16_t bfType;
  uint32_t bfileSize;
  uint32_t bfReserved;
  uint32_t bOffBits;
  uint32_t biSize;
  uint32_t biWidth;
  uint32_t biHeight;
  uint16_t biPlanes;
  uint16_t biBitCount;
  uint32_t biCompression;
  uint32_t biSizeImage;
  uint32_t biXPelsPerMeter;
  uint32_t biYPelsPerMeter;
  uint32_t biClrUsed;
  uint32_t biClrImportant;
};

enum READ_BMP_STATUS { READ_OK, READ_ERROR };

enum WRITE_BMP_STATUS { WRITE_OK, WRITE_ERROR };

enum READ_BMP_STATUS from_bmp(FILE *input_image_file, struct image *image);

enum WRITE_BMP_STATUS to_bmp(struct image *image, FILE *result_image_file);

#endif
