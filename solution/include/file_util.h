#ifndef FILE_UTIL_H
#define FILE_UTIL_H

#include <stdio.h>

enum OPEN_STATUS { OPEN_OK, OPEN_ERROR };

enum CLOSE_STATUS { CLOSE_OK, CLOSE_ERROR };

FILE *open_input_bmp_file(const char *name);

FILE *open_result_bmp_file(const char *name);

enum OPEN_STATUS open_status(FILE *file);

enum CLOSE_STATUS close_file(FILE *file);

#endif
