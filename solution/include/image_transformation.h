#ifndef IMAGE_TRANSFORMATION_H
#define IMAGE_TRANSFORMATION_H

#include "image.h"

struct image image_90_degree_rotation(struct image *image);

#endif
