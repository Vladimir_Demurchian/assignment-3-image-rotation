#ifndef IMAGE_H
#define IMAGE_H

#include <stdint.h>

struct pixel {
  uint8_t b, g, r;
};

struct image {
  uint64_t width, height;
  struct pixel *data;
};

struct pixel *pixel_calculate(const struct image *image, uint32_t x, uint32_t y);

struct image image_create(uint32_t width, uint32_t height);

void image_free(struct image *image);

#endif
