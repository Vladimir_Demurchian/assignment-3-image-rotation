#include "../include/file_util.h"
#include "../include/error_util.h"

FILE *open_input_bmp_file(const char *name) { return fopen(name, "rb"); }

FILE *open_result_bmp_file(const char *name) { return fopen(name, "wb"); }

enum OPEN_STATUS open_status(FILE *file) {
  if (file != NULL)
    return OPEN_OK;
  print_error("Failed to open image!");
  return OPEN_ERROR;
}

enum CLOSE_STATUS close_file(FILE *file) {
  if (fclose(file) != EOF)
    return CLOSE_OK;
  print_error("Error when closing!");
  return CLOSE_ERROR;
}
