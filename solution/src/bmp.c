#include "bmp.h"

#include "../include/error_util.h"
#include "../include/image.h"

uint8_t padding_calculate(uint32_t width) { return PADDING - (sizeof(struct pixel) * width) % PADDING; }

struct bmp_header header_fill(uint32_t width, uint32_t height) {
  const uint32_t header_size = sizeof(struct bmp_header);
  const uint32_t image_size = (sizeof(struct pixel) * width + padding_calculate(width)) * height;
  return (struct bmp_header){0x4D42, header_size + image_size, 0, header_size, 40, width, height, 1, 24, 0, image_size, 0, 0, 0,
                             0};
}

enum READ_BMP_STATUS from_bmp(FILE *input_image_file, struct image *image) {
  struct bmp_header bmp_header;
  if (fread(&bmp_header, sizeof(struct bmp_header), 1, input_image_file) != 1) {
    print_error("Error reading the header!");
    return READ_ERROR;
  }
  *image = image_create(bmp_header.biWidth, bmp_header.biHeight);
  if (image->width == 0) {
    image_free(image);
    print_error("Error when creating a structure for an image!");
    return READ_ERROR;
  }
  for (size_t height = 0; height < image->height; height++) {
    if (fread(pixel_calculate(image, 0, height), sizeof(struct pixel), image->width, input_image_file) != image->width ||
        fseek(input_image_file, padding_calculate(image->width), SEEK_CUR) != 0) {
      image_free(image);
      print_error("Error when reading image pixels!");
      return READ_ERROR;
    }
  }
  return READ_OK;
}

enum WRITE_BMP_STATUS to_bmp(struct image *image, FILE *result_image_file) {
  if (image == NULL || result_image_file == NULL) {
    print_error("Error when reading input data!");
    return WRITE_ERROR;
  }
  struct bmp_header bmp_header = header_fill(image->width, image->height);
  if (fwrite(&bmp_header, sizeof(struct bmp_header), 1, result_image_file) != 1) {
    print_error("Error writing headers!");
    return WRITE_ERROR;
  }
  for (size_t height = 0; height < image->height; height++) {
    uint8_t padding = padding_calculate(image->width);
    char garbage[PADDING] = "";
    if (fwrite(pixel_calculate(image, 0, height), sizeof(struct pixel), image->width, result_image_file) != image->width ||
        fwrite(&garbage, sizeof(uint8_t), padding, result_image_file) != padding) {
      print_error("Error writing pixels!");
      return WRITE_ERROR;
    }
  }
  return WRITE_OK;
}
