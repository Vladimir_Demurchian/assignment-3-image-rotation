#include <stdio.h>

#include "../include/bmp.h"
#include "../include/error_util.h"
#include "../include/file_util.h"
#include "../include/image.h"
#include "../include/image_transformation.h"

int main(int argc, char **argv) {
  if (argc != 3) {
    print_error("There must be 3 arguments!");
    return 1;
  }

  FILE *input_image_file = open_input_bmp_file(argv[1]);
  if (open_status(input_image_file) == READ_ERROR)
    return 1;

  struct image image;

  if (from_bmp(input_image_file, &image) == READ_ERROR)
    return 1;

  if (close_file(input_image_file) == CLOSE_ERROR)
    return 1;

  struct image reverse_image = image_90_degree_rotation(&image);

  image_free(&image);

  FILE *result_image_file = open_result_bmp_file(argv[2]);
  if (open_status(result_image_file) == READ_ERROR)
    return 1;

  if (to_bmp(&reverse_image, result_image_file) == WRITE_ERROR) {
    image_free(&reverse_image);
    close_file(result_image_file);
    return 1;
  }

  image_free(&reverse_image);

  if (close_file(result_image_file) == CLOSE_ERROR)
    return 1;
  return 0;
}
