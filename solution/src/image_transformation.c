#include "../include/image_transformation.h"
#include "../include/error_util.h"
#include "../include/image.h"

struct image image_90_degree_rotation(struct image *image) {
  struct image image_result = image_create(image->height, image->width);
  if (image_result.width == 0) {
    print_error("Error when creating a structure for an result image!");
    image_free(&image_result);
  }
  for (uint32_t y = 0; y < image->height; y++)
    for (uint32_t x = 0; x < image->width; x++)
      *pixel_calculate(&image_result, image->height - 1 - y, x) = *pixel_calculate(image, x, y);
  return image_result;
}
