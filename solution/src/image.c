#include <stdlib.h>

#include "../include/image.h"

struct image image_create(uint32_t width, uint32_t height) {
  struct pixel *pixels = malloc(sizeof(struct pixel) * width * height);
  if (pixels == NULL)
    return (struct image){0};
  else
    return (struct image){width, height, pixels};
}

void image_free(struct image *image) {
  image->width = 0;
  image->height = 0;
  free(image->data);
  image->data = NULL;
}

struct pixel *pixel_calculate(const struct image *image, uint32_t x, uint32_t y) { return y * image->width + x + image->data; }
